Для развертывания и тестирования API:

1. Установите необходимые зависимости, выполнив команду pip install -r req.txt
2. Создайте миграции для моделей с помощью команды python manage.py makemigrations и примените их с помощью команды python manage.py migrate
3. Запустите сервер API с помощью команды python manage.py runserver
4. Документация API доступна по адресу 
	- http://localhost:8000/
	- http://localhost:8000/swagger/
	- http://localhost:8000/redoc/
5. Используйте инструменты, такие как cURL или Postman, для отправки запросов к конечным точкам API (например, http://localhost:8000/users, http://localhost:8000/posts и т. д.)