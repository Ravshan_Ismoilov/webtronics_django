from django.urls import path, include
from rest_framework import routers
from mainapp.views import UserViewSet, PostViewSet, LikeViewSet

app_name = 'mainapp'

router = routers.DefaultRouter()
router.register('users', UserViewSet)
router.register('posts', PostViewSet)
router.register('likes', LikeViewSet)

urlpatterns = [
	path('', include(router.urls)),
]