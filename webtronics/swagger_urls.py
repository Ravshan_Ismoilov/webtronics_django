from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions
from django.urls import path, include

schema_view = get_schema_view(
    openapi.Info(
        title="Webtronics API",
        default_version='v1',
        description="Description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@info.local"),
        license=openapi.License(name="License"),
    ),
    patterns=[path('api/', include('mainapp.urls')), ],
    public=True,
    permission_classes=(permissions.AllowAny,),
)